// $0.getBoundingClientRect();
/* Si se ejecuta como metodo se objtiene u objeto que brinda
información útil sobre el cuadro y se pued ejecutar para cuelquier
elemento de la página, esto otorga coordenadas y tamaños*/

// $0.offsetTop
/* offsetTop le da la distancia del punto más alto a la parte superior
de la coordenada
*/

//offsetLeft
/* Son basicamente coordenadas en los valores x & y de los valores izquierda
y superior del objeto */

// $0.clientTop      $0.clientLeft
/*  Donde el desplazamiento le da el posicionamiento externo, por lo que la posición 
del elemento que se este viendo incluidos los bordes y barras de desplazamiento*/

//$0.offsetWidth offsetHeigh
/* Es el ancho  y alto del cuadro oh elemento que se este analizando, 
incluidos los bordes y barras de desplazamiento*/

//$0.clientWidh clientHeight
/* También es el ancho y el alto interno de pero este sin bordes y 
barras de desplazamiento */

/* --------------------- DESPLAZAMIENTO --------------------------------- */

// $0.scrollHeight
/* Es la altura completa del contenido, incluida la parte que no esta visible */

// $0.scrollTop
/* Da la información por cuánto desplazas el contenido */

// windows.innerWidth    windows.innerHeight
/* Se se busca obtener todo el ancho y el alto del documento
Si se tiene una barra de desplazamiento va incluido  */

//document.documentElement.clientWidth (or clientHeight)
/*Da el ancho y el alto total del documento desde la raíz incluido el
encabezado y cuerpo */